import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import AppBarLayout from './layouts/AppBarLayout';
import MainLayout from './layouts/MainLayout';
import {APP_BAR_LAYOUT_NAME, MAIN_LAYOUT_NAME} from './constants';
import {RootStackParamList} from './types';

const RootStack = createNativeStackNavigator<RootStackParamList>();

const App = () => {
  return (
    <>
      <NavigationContainer>
        <RootStack.Navigator>
          <RootStack.Screen
            name={APP_BAR_LAYOUT_NAME}
            options={{headerShown: false}}
            component={AppBarLayout}
          />
          <RootStack.Screen
            name={MAIN_LAYOUT_NAME}
            options={{headerShown: false}}
            component={MainLayout}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
