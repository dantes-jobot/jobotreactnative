import {AppRegistry} from 'react-native';
import {PaperProvider} from 'react-native-paper';
import {name as appName} from './app.json';
import App from './App';
import algoliasearch from 'algoliasearch';
import firebase from 'firebase/compat';
import {getFirestore} from 'firebase/firestore';
import initializeApp = firebase.initializeApp;
import {store} from './store';
import {Provider} from 'react-redux';

export const algoliaClient = algoliasearch(
  'GRQY4STLNB',
  'fb9501ac916527010b5bcea3aed91c2e',
);

const firebaseConfig = {
  apiKey: 'AIzaSyBgTDtA7qsWxQijT8qhLUj3zjbmggePJV0',
  authDomain: 'jobot-32a3b.firebaseapp.com',
  projectId: 'jobot-32a3b',
  storageBucket: 'jobot-32a3b.appspot.com',
  messagingSenderId: '523434051084',
  appId: '1:523434051084:web:0d2965c1de13b6eadd1029',
  measurementId: 'G-8BDSRTHJW9',
};

// Initialize Firebase
export const firebaseApp = initializeApp(firebaseConfig);
export const firestoreDB = getFirestore(firebaseApp);

export default function Main() {
  return (
    <Provider store={store}>
      <PaperProvider>
        <App />
      </PaperProvider>
    </Provider>
  );
}

AppRegistry.registerComponent(appName, () => Main);
