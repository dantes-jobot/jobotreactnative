export const CONTAINER_HORIZONTAL_PADDING = 20;
export const APP_BAR_LAYOUT_NAME = 'AppBarLayout';
export const MAIN_LAYOUT_NAME = 'MainLayout';

export const JOBS_SCREEN_NAME = 'Jobs';
export const SETTINGS_SCREEN_NAME = 'Settings';
export const JOB_DETAILS_SCREEN_NAME = 'JobDetails';

export const HITS_PER_PAGE = 30;
