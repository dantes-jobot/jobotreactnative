import {
  APP_BAR_LAYOUT_NAME,
  JOB_DETAILS_SCREEN_NAME,
  JOBS_SCREEN_NAME,
  MAIN_LAYOUT_NAME,
  SETTINGS_SCREEN_NAME,
} from './constants';
import {NavigatorScreenParams} from '@react-navigation/native';

export type RootStackParamList = {
  [APP_BAR_LAYOUT_NAME]: NavigatorScreenParams<AppBarLayoutTabParamList>;
  [MAIN_LAYOUT_NAME]: NavigatorScreenParams<MainLayoutParamList>;
};

export type AppBarLayoutTabParamList = {
  [JOBS_SCREEN_NAME]: undefined;
  [SETTINGS_SCREEN_NAME]: undefined;
};

export type MainLayoutParamList = {
  [JOB_DETAILS_SCREEN_NAME]: {
    id: string;
  };
};

export type JobDetailsRouteParamList = MainLayoutParamList;
