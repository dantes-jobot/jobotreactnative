import {Appbar} from 'react-native-paper';

type HeaderPropsType = {
  title: string;
};

const Header = (props: HeaderPropsType) => (
  <Appbar.Header elevated>
    <Appbar.Content title={props.title} />
  </Appbar.Header>
);

export default Header;
