import React from 'react';
import JobsScreen from '../screens/JobsScreen';
import {JOBS_SCREEN_NAME, SETTINGS_SCREEN_NAME} from '../constants';
import SettingsScreen from '../screens/SettingsScreen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

const Tab = createMaterialBottomTabNavigator();

const tabIconSize = 24;

const JobsIcon = () => <Icon name="work" size={tabIconSize} />;
const SettingsIcon = () => <Icon name="settings" size={tabIconSize} />;

const BottomNavBar = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name={JOBS_SCREEN_NAME}
        component={JobsScreen}
        options={{
          tabBarLabel: 'Jobs',
          tabBarIcon: JobsIcon,
        }}
      />
      <Tab.Screen
        name={SETTINGS_SCREEN_NAME}
        component={SettingsScreen}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: SettingsIcon,
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNavBar;
