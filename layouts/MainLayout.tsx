import {createNativeStackNavigator} from '@react-navigation/native-stack';
import JobDetailsScreen from '../screens/JobDetailsScreen';
import {JOB_DETAILS_SCREEN_NAME} from '../constants';

const MainLayoutStack = createNativeStackNavigator();

const MainLayout = () => {
  return (
    <>
      <MainLayoutStack.Navigator>
        <MainLayoutStack.Screen
          name={JOB_DETAILS_SCREEN_NAME}
          component={JobDetailsScreen}
        />
      </MainLayoutStack.Navigator>
    </>
  );
};

export default MainLayout;
