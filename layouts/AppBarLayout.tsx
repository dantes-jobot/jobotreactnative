import Header from '../components/Header';
import BottomNavBar from '../components/BottomNavBar';

const AppBarLayout = () => {
  return (
    <>
      <Header title="Jobot" />
      <BottomNavBar />
    </>
  );
};

export default AppBarLayout;
