import {Button, Text, View} from 'react-native';
import {centeredViewStyle} from '../styles';
import {useNavigation} from '@react-navigation/native';
import {JOB_DETAILS_SCREEN_NAME, MAIN_LAYOUT_NAME} from '../constants';

const SettingsScreen = () => {
  const navigation = useNavigation();
  const goToJobDetails = () => {
    navigation.navigate(MAIN_LAYOUT_NAME, {
      screen: JOB_DETAILS_SCREEN_NAME,
      params: {
        id: 'ascasnc',
      },
    });
  };
  return (
    <View style={centeredViewStyle.container}>
      <Text>Setting</Text>
      <Button title="Details" onPress={goToJobDetails} />
    </View>
  );
};

export default SettingsScreen;
