import {ScrollView, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {JOB_DETAILS_SCREEN_NAME, MAIN_LAYOUT_NAME} from '../constants';
import {useAppDispatch, useAppSelector} from '../app/hooks';
import {
  clearJobsList,
  fetchFirstJobsFromAlgolia,
  resetPagination,
  setCurrentJob,
} from '../store/JobsSlice';
import {useEffect} from 'react';
import {Divider, List} from 'react-native-paper';
import {Nullable} from '../types/nullable';

const JobsScreen = () => {
  const {jobsList, currentJob, jobsCount, loadingList} = useAppSelector(
    state => state.jobs,
  );
  const dispatch = useAppDispatch();
  const getFirstJobs = async () => {
    if (!jobsList.length) {
      dispatch(resetPagination());
      dispatch(clearJobsList());
      return dispatch(fetchFirstJobsFromAlgolia(null));
    }
    return null;
  };
  useEffect(() => {
    (async () => {
      await getFirstJobs();
    })();
  }, []);
  const navigation = useNavigation();
  const goToJobDetails = (id: Nullable<string>) => {
    if (!id) {
      return;
    }

    const foundJob = jobsList.find(j => j.id === id);
    if (foundJob) {
      dispatch(setCurrentJob(foundJob));
    }

    navigation.navigate(MAIN_LAYOUT_NAME, {
      screen: JOB_DETAILS_SCREEN_NAME,
      params: {
        id,
      },
    });
  };
  return (
    <View>
      <ScrollView>
        <>
          {jobsList.map((job, index) => (
            <>
              <List.Item
                onPress={() => goToJobDetails(job.id)}
                title={job.title}
                key={job.id}
                description={job.companyName}
              />
              {index !== jobsList.length - 1 ? <Divider /> : null}
            </>
          ))}
        </>
      </ScrollView>
    </View>
  );
};

export default JobsScreen;
