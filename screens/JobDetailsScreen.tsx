import {Text, View} from 'react-native';
import {centeredViewStyle} from '../styles';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import {JobDetailsRouteParamList} from '../types';
import {useEffect} from 'react';
import {useAppSelector} from '../app/hooks';

const JobDetailsScreen = () => {
  const route = useRoute<RouteProp<JobDetailsRouteParamList>>();
  const {currentJob} = useAppSelector(state => state.jobs);
  const navigation = useNavigation();
  useEffect(() => {
    navigation.setOptions({title: currentJob?.title});
  }, [route.params.id]);
  return (
    <View style={centeredViewStyle.container}>
      <Text>{currentJob?.title}</Text>
    </View>
  );
};

export default JobDetailsScreen;
