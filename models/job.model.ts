import { Nullable } from '../types/nullable';

export type JobModelBase = {
  provider: string;
  type: 'freelance' | 'longTerm',
  originalId: string | null;
  originalApplyUrl: string | null;
  tags: string[] | [];
  title: string | null;
  date: string | null;
  salaryRange: string | null;
  description: string | null;
  companyLogo: string | null;
  companyName: string | null;
}

export type JobModel = JobModelBase & {
  id: Nullable<string>;
}

export type JobModelFromAlgolia = JobModelBase & {
  objectID: Nullable<string>;
}
