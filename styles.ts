import {StyleSheet, ViewStyle} from 'react-native';
import {CONTAINER_HORIZONTAL_PADDING} from './constants';

export const viewContainerStyle: ViewStyle = {
  padding: CONTAINER_HORIZONTAL_PADDING,
};

export const centeredViewStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
