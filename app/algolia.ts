import {algoliaClient} from '../index';
import {JobModel, JobModelFromAlgolia} from '../models/job.model';

export type SearchRequest = {
  query: string;
  page: number;
  hitsPerPage: number;
};

export type SearchResponse = {
  hits: JobModel[];
  nbHits: number;
  page: number;
  nbPages: number;
  query: string;
};

export default {
  search: async ({
    query = '',
    page = 0,
    hitsPerPage = 30,
  }: SearchRequest): Promise<SearchResponse> => {
    const index = algoliaClient.initIndex('jobs');
    const res = await index.search<JobModelFromAlgolia>(query, {
      hitsPerPage,
      page,
    });
    return {
      query: res.query,
      nbPages: res.nbPages,
      nbHits: res.nbHits,
      page: res.page,
      hits: res.hits.map(hit => ({
        ...hit,
        id: hit.objectID,
      })),
    };
  },
};
