import { doc, getDoc } from 'firebase/firestore';
import { firestoreDB } from '../index';
import { JobModel } from '../models/job.model';

export default {
  async getJob(jobId: string) {
    const docRef = doc(firestoreDB, '/jobs', jobId);
    const docData = await getDoc(docRef);
    return {
      ...docData.data(),
      id: docData.id,
    } as JobModel;
  },
};
