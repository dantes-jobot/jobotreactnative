import { JobModel } from '../models/job.model';

export type JobsState = {
  jobsList: JobModel[],
  page: number,
  hitsPerPage: number,
}
