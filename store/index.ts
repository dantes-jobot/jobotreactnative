import {configureStore} from '@reduxjs/toolkit';
import {jobsSlice} from './JobsSlice';

export const store = configureStore({
  reducer: {
    jobs: jobsSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
