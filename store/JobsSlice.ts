import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';
import {JobModel} from '../models/job.model';
import {Nullable} from '../types/nullable';
import algolia, {SearchRequest, SearchResponse} from '../app/algolia';
import {HITS_PER_PAGE} from '../constants';
import {RootState} from './index';

const jobsAdapter = createEntityAdapter();

const initialState = jobsAdapter.getInitialState({
  jobsList: [] as JobModel[],
  currentJob: null as Nullable<JobModel>,
  loadingList: true,
  loadingListByFilters: true,
  searchParams: {
    query: '',
    page: 0,
    hitsPerPage: HITS_PER_PAGE,
  } as SearchRequest,
  loadingNextJobs: false,
  showingLoaderNextJobs: true,
  loadingJobDetails: true,
  jobsCount: 0,
});

export const fetchFirstJobsFromAlgolia = createAsyncThunk<
  Nullable<SearchResponse>,
  unknown,
  {
    state: RootState;
  }
>('jobs/fetchFirstJobsFromAlgolia', async (params, {getState, dispatch}) => {
  dispatch(clearJobsList());

  dispatch(resetPagination());
  return algolia.search({
    ...getState().jobs.searchParams,
  });
});
export const fetchNextJobsFromAlgolia = createAsyncThunk<
  Nullable<SearchResponse>,
  unknown,
  {
    state: RootState;
  }
>('jobs/fetchNextJobsFromAlgolia', async (params, {getState, dispatch}) => {
  if (
    !getState().jobs.loadingNextJobs &&
    getState().jobs.jobsList.length < getState().jobs.jobsCount
  ) {
    dispatch(toggleLoadingNextJobs(true));
    return algolia.search({
      ...getState().jobs.searchParams,
    });
  }
  return Promise.resolve(null);
});
export const jobsSlice = createSlice({
  name: 'jobs',
  initialState,
  extraReducers: builder => {
    builder
      .addCase(fetchFirstJobsFromAlgolia.pending, state => {
        state.loadingList = true;
      })
      .addCase(fetchFirstJobsFromAlgolia.fulfilled, (state, action) => {
        state.jobsList = action.payload?.hits || [];
        state.loadingList = false;
        state.jobsCount = action.payload?.nbHits || 0;
        if (action.payload?.nbPages || 0 > 1) {
          state.searchParams.page += 1;
        } else {
          state.showingLoaderNextJobs = false;
        }
      })
      .addCase(fetchNextJobsFromAlgolia.fulfilled, (state, action) => {
        state.jobsList.push(...(action.payload?.hits || []));
        state.loadingNextJobs = false;
        state.searchParams.page += 1;
      });
  },
  reducers: {
    setCurrentJob: (state, action: PayloadAction<Nullable<JobModel>>) => {
      state.currentJob = action.payload;
    },
    toggleLoadingJobDetails: (state, action: PayloadAction<boolean>) => {
      state.loadingJobDetails = action.payload;
    },
    toggleLoadingNextJobs: (state, action: PayloadAction<boolean>) => {
      state.loadingNextJobs = action.payload;
    },
    toggleLoadingListByFilters: (state, action: PayloadAction<boolean>) => {
      state.loadingListByFilters = action.payload;
    },
    resetPagination: state => {
      state.searchParams.page = 0;
    },
    clearJobsList: state => {
      state.jobsList = [];
    },
    setSearchQuery: (state, action: PayloadAction<string>) => {
      state.searchParams.query = action.payload;
    },
  },
});

export const {
  setCurrentJob,
  toggleLoadingJobDetails,
  toggleLoadingNextJobs,
  resetPagination,
  clearJobsList,
  setSearchQuery,
} = jobsSlice.actions;
